﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_1
{
    class Program
    {
        static void Print(string str)
        {
             Console.WriteLine(str+"\n");
        }
        static void Main(string[] args)
        {
            Print("Andriy Sysak's homework_1 \n----------------------------");
            Print("Write a command !");
            Print("info                 - to watch info about me(name and surname) ");
            Print("IT_lvl               - to watch info about my lvl in IT field");
            Print("quit                 - to quit ");

            while(true)
            {
                string cmd = Console.ReadLine();

                if(cmd.ToLower() == "info")
                {
                    Print("Name - Andriy \nSurname - Sysak");
                }

                else if(cmd.ToLower() == "quit")
                {
                    break;
                }

                else if(cmd.ToLower() =="it_lvl")
                {
                    Print("C/C++        - intermediate + OOP");
                    Print("Python       - basic");
                    Print("Java         - intermediate + OOP");
                    Print("C#           - almost basic");
                    Print("Pascal       - basic");
                }

                else
                {
                    Print("Please try again !");
                }
            }
        }
    }
}
